//
//  AddWordViewController.swift
//  Lasagna
//
//  Created by Crazy Developer on 66.66.666.
//

import UIKit

protocol AddWordViewControllerDelegate {
    func addWordViewControllerDidFinish(text1: String, text2: String)
}

class AddWordViewController: UIViewController {
    var textField1: UITextField!
    var textField2: UITextField!
    var saveButton: UIButton!
    
    var delegate: AddWordViewControllerDelegate!
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        textField1 = UITextField()
        textField1.backgroundColor = .green
        textField1.frame = CGRect(x: 0, y: 100, width: view.bounds.size.width, height: 30)
        view.addSubview(textField1)
        textField2 = UITextField()
        textField2.backgroundColor = .green
        textField2.frame = CGRect(x: 0, y: 150, width: view.bounds.size.width, height: 30)
        view.addSubview(textField2)
        saveButton = UIButton()
        saveButton.backgroundColor = .red
        saveButton.addTarget(self, action: #selector(actionSave), for: .touchUpInside)
        saveButton.frame = CGRect(x: 0, y: 200, width: view.bounds.size.width, height: 44)
        view.addSubview(saveButton)
    }
    
    @objc func actionSave() {
        delegate.addWordViewControllerDidFinish(text1: textField1.text!, text2: textField2.text!)
    }
}
