//
//  MainViewController.swift
//  Lasagna
//
//  Created by Crazy Developer on 66.66.666.
//

import UIKit
import RealmSwift

class Word: Object {
    @objc dynamic var spelling: String!
    @objc dynamic var translation: String!
}

class WordCell: UITableViewCell {
    var label1: UILabel!
    var label2: UILabel!
    var deleteButton: UIButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        label1 = UILabel()
        addSubview(label1)
        label1.frame = CGRect(x: 0, y: 0, width: frame.size.width/2, height: frame.size.height)
        
        label2 = UILabel()
        addSubview(label2)
        label2.frame = CGRect(x: frame.size.width/2, y: 0, width: frame.size.width/2, height: frame.size.height)
        
        deleteButton = UIButton()
        deleteButton.backgroundColor = .red
        deleteButton.frame = CGRect(x: UIScreen.main.bounds.size.width-44, y: 0, width: 44, height: 44)
        contentView.addSubview(deleteButton)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MainViewController: UIViewController, UITableViewDataSource, AddWordViewControllerDelegate {
    var words: [Word]!
    var tableView = UITableView()
    var realm = try! Realm(fileURL: Bundle.main.url(forResource: "default", withExtension: "realm")!)
    
    override func viewDidLoad() {
        tableView.dataSource = self
        tableView.frame = view.frame
        view.addSubview(tableView)
        
        words = Array(realm.objects(Word.self))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(actionAdd))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = WordCell()
        cell.label1.text = words[indexPath.row].spelling
        cell.label2.text = words[indexPath.row].translation
        cell.deleteButton.addTarget(self, action: #selector(actionDelete(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func actionAdd() {
        let vc = AddWordViewController()
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addWordViewControllerDidFinish(text1: String, text2: String) {
        let word = Word()
        word.spelling = text1
        word.translation = text2
        realm.beginWrite()
        realm.add(word)
        try! realm.commitWrite()
        words = Array(realm.objects(Word.self))
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func actionDelete(_ sender: UIButton) {
        let index = tableView.indexPath(for: sender.superview?.superview as! UITableViewCell)!.row
        let result = realm.objects(Word.self).filter("spelling = %@", words[index].spelling!)
        realm.beginWrite()
        realm.delete(result)
        try! realm.commitWrite()
        words = Array(realm.objects(Word.self))
        tableView.reloadData()
    }
}
